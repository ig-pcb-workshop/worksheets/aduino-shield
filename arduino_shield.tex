\documentclass[letterpaper]{article}
\usepackage{tikz}
\usepackage{circuitikz}
\usepackage{graphicx}
\usepackage[margin=0.8in]{geometry}

\begin{document}
	
	\title{Ithaca Generator KiCAD PCB Workshop\\ \large{Arduino Shield Project}}
	\date{}
	\maketitle
	
	\section{Overview}
	\paragraph{} Welcome to the Ithaca Generator Circuit Board Workshop! 
	This is the worksheet for the Arduino Shield project. This is a great project to get started with learning how to get started with PCB design if you're just getting familiar with electronics, or have been using arduinos for a while and want to make your own shield for a project!
	Please note that you may not finish your design within the allotted time of the workshop, so feel free to come again on any other Saturday for help if you need it. This project was designed to be very simple, but do not worry if it is giving you trouble. Don't hesitate to ask for help if you need it! Feel free to use this worksheet as a 'cheat-sheet' for resources in your future designs. The design shown here is also a reference, meaning you can feel free to modify it to your liking.
	
	\paragraph{} By the end of this workshop, you should have basic skills to be able to design a circuit board from scratch, and get it manufactured. This workshop will not talk about circuit design, only circuit board design; in other words, you will be able to implement any circuit you design or come across online. There will be minimal discussion of electronics theory, but only as rules of thumb that will apply to circuit board design.
	
	
	\pagebreak
	\subsection{Project Description}
	\paragraph{} This project allows you to make a device that reads a temperature sensor, and then displays the current temperature on the LED bar graph. There are no numerical outputs, only relative like that on a car's temperature readout. In order to control all the LEDs on the bar graph, an IO expander chip is used. The temperature sensor and IO expander both have Arduino libraries and are super common parts, so no need to write your own drivers from scratch!
	
	\paragraph{} Figure \ref{fig:tmp_sensor} shows the LM35 temperature sensor used. It is a simple analog temperature sensor, that outputs between 0 and 5V for the corresponding temperature. The datasheet for the LM35 will show in detail the temperature curve to convert the voltage into the current temperature; though the Arduino library will also do that for you if you feel lost.
		% Temperature Sensor	
	
	\begin{figure}[h!]
		\centering
		\caption[Speaker Schematic]{}
		\label{fig:tmp_sensor}
		\includegraphics[width=0.3\paperwidth]{Temp_Sensor}
	\end{figure}
	
	\paragraph{} Figure \ref{fig:bargraph} shows the IO expander and bargraph. Note all the $220\Omega$ resistors (R3 through R12): they are there to provide a current limit so you don't burn out the LEDs or the IO expander. The IO expander is U2, the PCF8574. It is a very common IO expander that uses the I2C serial bus to communicate with the arduino. R1 and R2 are very important for the operation of I2C, as there is no other voltage source. The I2C pins can only pull the voltage to 0V, not up to the supply voltage (5V in this case). For reference, SDA is the bidirectional data signal (the io Arduino will send and recieve data on this pin), and SCL is the clock (tells the devices when data is valid). You don't need to worry too much about the I2C bus, this is just providing some background into what is going on.
	
	% LED Bar graph
	
	\begin{figure}[h!]
		\centering
		\caption[Speaker Schematic]{}
		\label{fig:bargraph}
		\includegraphics[width=0.8\paperwidth]{LED_Bar}
	\end{figure}
	
	
	\clearpage	
	\paragraph{} This is the full schematic; please reference this while working so you have reference if you get stuck!
	\begin{figure}[h!]
		\centering
		\rotatebox[origin=c]{90}{
			\includegraphics[width=\paperwidth]{full_schematic}
		}
		\caption[Speaker Schematic]{}
		\label{fig:fullschematic}
	\end{figure}
	\clearpage

	
\section{Schematic Capture}
\paragraph{} EESchema is the program in KiCAD for creating schematics. Don't forget to create a new project first! This is where you will draw out your circuit in a representation that is easy to read, compared with circuit boards. Below are some things to keep in mind when creating a schematic.

\begin{itemize}
	\item Label your nets! The shortcut is the "L" key. This makes it much easier to keep track of your nets on the schematic and PCB. Also makes DRC rules easier to set up
	\item Put notes for your decisions near components. Just like with programming, you will forget what you did and why you did it. 
	\item For complex designs, using hierarchical schematic sheets makes it easier to keep track of your designs
	\item Hierarchical sheets are also modular; you can reuse them in other projects, or multiple times in the same project!
	\item Using SPICE simulation, you can simulate your circuit. This definitely helps when making analog designs to give you an idea of performance. KiCAD natively supports simulation in EESchema.
	\item Don't assume the parts in the KiCAD library are still available; there are a lot of obsolete parts (no longer manufactured). Especially with the chip shortages, make sure to check what components are available before spending time on a design!
	\item Use labels to connect wires across a schematic. Drawing wires across the page may look cool, but it ends up becoming illegible once you have a lot of traces.
\end{itemize}

\clearpage
\section{Routing and Layout}
\paragraph{} PCBNew is the program in KiCAD for drawing the actual circuit board. When first creating your board, or after you make changes, don't forget to import the changes by clicking the "Update PCB From Schematic"(Figure \ref{fig:sch2pcb}) button or by going to Tools/Update PCB From Schematic in the menu.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.05\paperwidth]{sch2pcb}
	\caption{Update PCB From Schematic Button}
	\label{fig:sch2pcb}
\end{figure}

\paragraph{} Here are some tips for layout and routing
\begin{itemize}
	\item Plan your layout carefully: a good layout makes routing tracks much easier.
	\item Check that the part values you want are available in the package sizes you want. You won't be able to find a SMD capacitor with a capacitance of 4700uF in a 0603 package. 
	\item Setup your rules and grid sizes before you begin your layout. They are critical to have error free designs before you even begin laying down traces.
	\item DRC rules have been made more extensible in KiCAD 6 with the scripting option. Use the help tab in the editor window as reference.
	\item Make net classes for your signals. This was touched upon in the last section. Power nets, high voltage nets, analog, digital, high speed signals, etc. 
	\item Through hole components usually use imperial units, surface mount components usually are metric, trace sizes are usually imperial, and board sizes are usually metric. Get used to flipping between units as it will be important. I tend to leave the grid in metric.
	\item Check the 3D view now and then to see if you did anything dumb, like place a component inside another one. Sometimes the 2D view is a little confusing.
\end{itemize}


\subsection{Layers}
\paragraph{} There are quite a few layers in PCBNew, and their names aren't always that easy to decipher. Here are a list of the ones that you will be using often, and their meanings.

\vspace{1cm}

\begin{tabular}{|c|c|c|}
	\hline
	Layer & Meaning & Use \\
	\hline
	F.Cu & Top Copper Layer & Routing \\
	\hline
	B.Cu & Bottom Copper Layer & Routing \\
	\hline
	F.Silkscreen & Top Silkscreen Layer & Text and images \\
	\hline
	B.Silkscreen & Bottom Silkscreen Layer & Text and images \\
	\hline
	Edge.Cuts & Board outline & Define the outline of the board \\
	\hline
	F.Courtyard & Top Layer Component Bounding Box & Determine spacing between components \\
	\hline
	B.Courtyard & Bottom Layer Component Bounding Box & Determine spacing between components \\
	\hline
	In.1 & Inner Copper Layer 1 & Routing \\
	\hline
	In.2 & Inner Copper Layer 2 & Routing \\
	\hline
	F.Fab & Top Layer Fabrication Notes & Fabrication notes, component placement \\
	\hline
	B.Fab & Bottom Layer Fabrication Notes & Fabrication notes, component placement \\
	\hline
\end{tabular}

\clearpage
\section{Custom Parts}
\paragraph{} Now not all parts are in KiCAD, and at some point you'll have to make your own symbol and footprint. This project uses components that are all found in the library that is installed when you install KiCAD, so no need to worry about having to do this during the workshop. The first thing to remember is that the datasheet is your friend; all information on the footprint, pinout, and maybe even a reference schematic is all found in a datasheet. Here's an example from the PCF8574, which is the IO expander chip.

\paragraph{} Figure \ref{fig:footprint} shows the dimensions recommended by the manufacturer of the PCF8574 for the pads and their spacing. Sometimes the datasheet will be a bit weird with how they represent dimensions, but I've only found connectors to be annoying. Though this package and many others are already in the KiCAD library! So don't forget to check to see what is already in the default library before making your own.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\paperwidth]{footprint}
	\caption{PCF8574 reference footprint}
	\label{fig:footprint}
\end{figure}


\paragraph{} Figure \ref{fig:symbol} shows the pinout of the physical chip. Keep in mind that some chips can be offered in multiple different package types that may have different pinouts from one another! 

\begin{figure}
	\centering
	\includegraphics[width=0.5\paperwidth]{symbol}
	\caption{PCF8574 physical pinout}
	\label{fig:symbol}
\end{figure}

\subsection{Downloading Libraries}
\paragraph{} There is also the possibility to download some premade libraries online. SnapEDA, UltraLibrarian, and many other services have parts that can be imported directly into KiCAD. The biggest issue there, is that the quality varies. Sometimes the downloaded library part isn't quite correct, and it is usually better to make your own from scratch. So use at your own risk. 


\clearpage
\section{Manufacturing}

\subsection{Where to find parts}
\paragraph{} Parts distributors are the go-to place to buy what you need for your project. Radioshack is a distant memory at this point, but do not fear since Radioshack had a terribly large markup on parts. Need 100 LEDs and through hole resistors to accompany them? Might be about \$8. If you did that at Radioshack before they went out, you would be shelling out over a \$100 for the same components. 

\begin{center}
	\begin{tabular}{|l|l|l|}
		\hline
		Company Name & Website & Comment \\
		\hline
		Mouser & https://mouser.com &  \\
		\hline
		Digi-Key & https://digikey.com &  \\
		\hline
		eBay/Alibaba & https://ebay.com / https://alibaba.com &  \\
		\hline
	\end{tabular}
\end{center}

\subsection{Preparing your design for manufacturing}
\paragraph{} Once you have a design that is complete and error free, you can finally create your fabrication files. Gerber files are the defacto standard for manufacturing PCBs. To generate the gerber files, go to the menu and select "File/Fabrication Outputs/Gerbers". Each board house will want different settings checked off, and most of them will have guides for KiCAD specifically. Follow their instructions as the settings are unfortunately not universal. Once the settings are selected, you can click the "plot" button. Don't forget to also generate the drill files! Their settings are also different for each board house. I have given the examples for what I use with PCBGOGO in the images below. The board house you use will most likely be similar to these, but will sometimes not work. You'll get an email saying to regenerate your files, and it will delay you getting your boards. Not a fun time. The X2 format for the gerbers isn't widely used yet, but the features that it contains is vastly better than older gerber file formats so board houses should support it sometime soon. PCBGOGO does as they asked me explicitly to set it, but they are the only board house I know of that uses it so far.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\linewidth]{gerber_settings}
	\caption{Gerber Settings}
	\label{fig:gerbersettings}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\linewidth]{drill_settings}
	\caption{Drill Settings}
	\label{fig:drillsettings}
\end{figure}

\paragraph{} If you also are using an assembly or turn-key service, make sure to generate the component position file. This will allow the manufacturer to place the components for you. To do this, just select the menu "File/Fabrication Outputs/Component Placement". If you have components on the top and bottom of the board, splitting up the files into top and bottom is probably the setting you want. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\linewidth]{cmp_pos}
	\caption[]{Component Position Files}
	\label{fig:cmppos}
\end{figure}


\clearpage

\subsection{Bill of Materials}
\paragraph{}My least favorite part of circuit board design, is by far the BOM or bill of materials. You have your design, now you need to find all the parts and put them into one file such that you can order everything. This is also a critical step for assembly/turn-key, as the manufacturer will have requirements for the BOM formatting. Thankfully if you put all the components part numbers into the parts in the schematic (double clicking them to edit the parameters), you can use the built in python scripts to generate the BOM. Or you can mass edit them by clicking on the table icon in the toolbar. 
\paragraph{}This part of the process can be annoying, especially with the chip shortage right now. There may be everything you want in stock today, but tomorrow they all disappear. Happened to me where there were over 2.5Million resistors for a specific part number. The next day when I went to order the parts, every single one was gone. Thankfully most passive components, and some chips, can be swapped with other part numbers. This process is extremely tedious unfortunately.

\clearpage

\subsection{Getting your board manufactured}
\paragraph{} In the past decade and a half, it has become extremely cheap to get your circuit board made. Below is a list of different board houses. It is by no means a complete list, but will get you started.

\begin{center}
	\begin{tabular}{|c|c|c|c|c|}
		\hline
		Board House & Website & Cost & Location & Time \\
		\hline
		OSHPark & https://oshpark.com & \$\$ & USA & \~{}2 Weeks \\
		\hline
		PCBGOGO & https://pcbgogo.com & \$\$ & China & 24 hours to a week \\
		\hline
		JLCPCB & https://jlcpcb.com & \$ & China & \~{} 1 Week \\
		\hline
		PCB:NG & https://www.pcbng.com/ & \$\$\$ & US & \~{} 2 Weeks \\
		\hline
	\end{tabular}
\end{center}

\paragraph{} Some things to keep in mind when choosing a board house:
\begin{itemize}
	\item If you use a Chinese board house, check their scheduling to make sure you don't order a board during the Chinese New Year as you will end up waiting pretty much the entirety of February to get your design back
	\item Assembly options are not available at every board house. This may not matter if you plan on soldering by hand. Assembly services will often cost much much more than the PCBs themselves.
	\item Remember to read the capabilities of the board house when setting your design rules. Going right up to the smallest feature that the board house is capable of will often result in errors and/or higher cost. 
	\item The smaller the pads for an IC, the smaller the spacing between traces, smaller the drill sizes, and the more layers used, the higher the cost will be. 2 and 4 layers tend to be about the same cost, while 6 and higher will definitely make them more expensive.
	\item Turn-Key means that the board house will buy the parts, make the circuit boards, and solder them for you. Very nice for large quantities of assemblies, but will cost a decent amount for low volumes.
\end{itemize}

	
	
\end{document}
